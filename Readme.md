# ESP Led Controller

This Project makes WS2812 Led Strips addressable through the Art-Net protocol using an ESP8266. 

Every Led of the strip listens to three consecutive Art-Net channels (RGB). 
The Controller is configured through the serial monitor and makes the settings persistent through its flash memory. 

### Usage

Requires [PlatformIO](https://docs.platformio.org/en/latest//core/installation/index.html).

upload + configure through 
```
pio run -t upload && pio device monitor
```
