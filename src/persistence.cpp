#include <EEPROM.h>
#include <Arduino.h>
#include "persistence.h"

void clearSerialInput() {
    delay(100);    // Make sure all chars arrived
    while (Serial.available() > 0)
        Serial.read();
}

void askQuestion (const char* question, char* buffer, size_t size) {
    clearSerialInput();
    Serial.print(question);
    String ret = Serial.readStringUntil('\n');
    ret.trim();
    ret.toCharArray(buffer, size);
    Serial.println(buffer);
}

int askQuestion (const char* question) {
    clearSerialInput();
    Serial.print(question);
    String ret = Serial.readStringUntil('\n');
    ret.trim();
    Serial.println(ret);
    return ret.toInt();
}

bool clearFlash() {
    for (int i=0; i < EEPROM.length(); i++)
        EEPROM.write(i, 0);
    return EEPROM.commit();
}

void printFlash() {
    for(int i =0; i < EEPROM.length(); i++) {
        Serial.printf("%i ", EEPROM.read(i));
        if ((i + 1) % 16 == 0)
            Serial.print("\r\n");
    }
}