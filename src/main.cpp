#include <ArtnetnodeWifi.h>
#include <Arduino.h>
#include <FastLED.h>
#include <EEPROM.h>
#include "persistence.h"

// Settings
char ssid[32];
char password[64];
char name[32];
int maxMilliwatts;
int startingUniverse;
int numberOfStrips;
int* lenthsOfStrips;

// LED vars
#define LED_PIN D4
int number_of_leds = 0;
CRGB* leds;

// Artnet vars
ArtnetnodeWifi artnetnode;

bool ConnectWifi()
{
    boolean state = true;
    int i = 0;

    Serial.printf("Connecting to WiFi \"%s\"\n", ssid);
    WiFi.begin(ssid, password);

    // Wait for connection
    Serial.print("Connecting");
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
        if (i > 20){
            state = false;
            break;
        }
        i++;
    }
    if (state) {
        Serial.println("");
        Serial.print("Connected to ");
        Serial.println(ssid);
        Serial.print("IP address: ");
        Serial.println(WiFi.localIP());
    } else {
        Serial.println("");
        Serial.println("Connection failed.");
    }

    return state;
}

void onDmxFrame(uint16_t univ, uint16_t len, uint8_t seq, uint8_t* data)
{
    if (univ < startingUniverse || univ > (startingUniverse + numberOfStrips - 1))
        return;

    /*Serial.printf("\n%x, %x, %x\n", univ, len, seq);
    for (int i = 0; i < len; ++i)
        Serial.printf(" %x", data[i]);*/


    // skip strips before current universe
    int offset = 0;
    for (int i = 0; i < univ - startingUniverse; ++i)
        offset += lenthsOfStrips[i];

    for (int i = 0; i < len && i < lenthsOfStrips[univ - startingUniverse] * 3; i++)
        leds[offset + (i / 3)][i % 3] = data[i];

    FastLED.show();
}

void setup()
{
    Serial.begin(74880);

    Serial.print("Press any key to enter setup ... \n");
    Serial.setTimeout(10000);
    char b[1];

    if (Serial.readBytes(b, 1) > 0) {
        Serial.setTimeout(-1);
        Serial.println("Welcome to Setup.");

        askQuestion("enter the ssid: ", ssid, sizeof(ssid));
        askQuestion("enter the password: ", password, sizeof(password));
        askQuestion("enter the name: ", name, sizeof(name));
        maxMilliwatts = askQuestion("enter the maximum milli watts: ");
        startingUniverse = askQuestion("enter the starting universe: ");
        numberOfStrips = askQuestion("enter the number of led strips: ");

        free(lenthsOfStrips);
        lenthsOfStrips = (int*) malloc(sizeof(int) * numberOfStrips);
        for (int i = 0; i < numberOfStrips; ++i)
            lenthsOfStrips[i] = askQuestion("enter length of Nth strip: ");


        EEPROM.begin(512);
        int addr = 0;
        clearFlash(); //TODO: remove
        EEPROM.write(511, 0xaa); // TODO: remove
        EEPROM.put(addr, ssid);
        addr += sizeof(ssid);
        EEPROM.put(addr, password);
        addr += sizeof(password);
        EEPROM.put(addr, name);
        addr += sizeof(name);
        EEPROM.put(addr, maxMilliwatts);
        addr += sizeof(maxMilliwatts);
        EEPROM.put(addr, startingUniverse);
        addr += sizeof(startingUniverse);
        EEPROM.put(addr, numberOfStrips);
        addr += sizeof(numberOfStrips);
        for (int i = 0; i < numberOfStrips; ++i) {
            EEPROM.put(addr, lenthsOfStrips[i]);
            addr += sizeof(lenthsOfStrips[i]);
        }
        EEPROM.commit();
        EEPROM.end();
    }

    // Load settings from flash
    EEPROM.begin(512);
    printFlash();
    int addr = 0;
    Serial.println("Recovered settings:");

    EEPROM.get(addr, ssid);
    Serial.printf("ssid: \"%s\"\n", ssid);
    addr+= sizeof(ssid);

    EEPROM.get(addr, password);
    Serial.printf("password: \"%s\"\n", password);
    addr+= sizeof(password);

    EEPROM.get(addr, name);
    Serial.printf("name: \"%s\"\n", name);
    addr+= sizeof(name);

    EEPROM.get(addr, maxMilliwatts);
    Serial.printf("maximum milli watts: %i\n", maxMilliwatts);
    addr+= sizeof(maxMilliwatts);

    EEPROM.get(addr, startingUniverse);
    Serial.printf("starting universe: %i\n", startingUniverse);
    addr+= sizeof(startingUniverse);

    EEPROM.get(addr, numberOfStrips);
    Serial.printf("number of strips: %i\n", numberOfStrips);
    addr+= sizeof(numberOfStrips);

    lenthsOfStrips = (int*) malloc(sizeof(int) * numberOfStrips);
    for (int i = 0; i < numberOfStrips; ++i) {
        EEPROM.get(addr, lenthsOfStrips[i]);
        Serial.printf("length of %ith strip: %i\n", i, lenthsOfStrips[i]);
        addr += sizeof(lenthsOfStrips[i]);
    }
    EEPROM.end();

    // Allocate led array
    for (int i = 0; i < numberOfStrips; ++i)
        number_of_leds += lenthsOfStrips[i];
    leds = (CRGB*) malloc(sizeof(CRGB) * number_of_leds);

    FastLED.setCorrection(TypicalPixelString);
    FastLED.setMaxPowerInMilliWatts(maxMilliwatts);
    set_max_power_indicator_LED(0);
    FastLED.setBrightness(255);
    FastLED.clear(true);
    FastLED.addLeds<WS2812, LED_PIN, GRB>(leds, number_of_leds);

    artnetnode.setName(name);
    artnetnode.setNumPorts(1);
    artnetnode.enableDMXOutput(0);
    artnetnode.setArtDmxCallback(onDmxFrame);

    ConnectWifi();
    artnetnode.begin(name);
}

void loop() {
    artnetnode.read();
}
